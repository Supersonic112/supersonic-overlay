# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

EGIT_REPO_URI="https://github.com/mosra/corrade"

inherit cmake-utils git-r3 ninja-utils

DESCRIPTION="C++11 Multiplatform plugin management and utility library"
HOMEPAGE="http://magnum.graphics/corrade/"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=""

DEPEND="${RDEPEND}
	dev-util/cmake
	dev-util/ninja"

CMAKE_MAKEFILE_GENERATOR="ninja"

src_configure() {
	local mycmakeargs=(
		-DCMAKE_INSTALL_PREFIX="${EPREFIX}/usr"
		-DCMAKE_BUILD_TYPE=Release
		-G Ninja
	)
	BUILD_DIR=${WORKDIR}/${PF} cmake-utils_src_configure
}

src_compile() {
	eninja
}

src_install() {
	DESTDIR="${D}" eninja install
}
