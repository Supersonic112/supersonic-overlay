# Copyright 1999-2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

EGIT_REPO_URI="https://github.com/mosra/magnum-plugins.git"

inherit cmake-utils git-r3 ninja-utils

DESCRIPTION="Plugins for the Magnum C++11/C++14 graphics engine"
HOMEPAGE="http://magnum.graphics"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+assimp
basisuniversal
+dds
+exr
+gex
+devil
+flac
+freetype
+harfbuzz
+jpeg
+png
+tinygltf
+vorbis
+wav
"

#TODO add as soon as available basisuniversal? ( media-libs/basis-universal )
RDEPEND="
	dev-libs/corrade
	dev-libs/magnum
	assimp? ( media-libs/assimp )
	devil? ( media-libs/devil )
	flac? ( media-libs/flac )
	freetype? ( media-libs/freetype )
	harfbuzz? ( media-libs/harfbuzz )
	jpeg? ( virtual/jpeg )
	png? ( media-libs/libpng )
	vorbis? ( media-libs/libvorbis )
"
#wav? ( media-libs/dr_libs??? )
DEPEND="${RDEPEND}
	dev-util/ninja"

CMAKE_MAKEFILE_GENERATOR="ninja"

src_configure() {
	# general configuration
	#local mycmakeargs=(
	mkdir -p "${WORKDIR}/${PF}/build"
	cd "${WORKDIR}/${PF}/build"
	cmake .. \
		-DCMAKE_INSTALL_PREFIX="${EPREFIX}/usr" \
		-DCMAKE_BUILD_TYPE=Release \
		-DWITH_ASSIMPIMPORTER="$(usex assimp ON OFF)" \
		-DWITH_BASISIMAGECONVERTER="$(usex basisuniversal ON OFF)" \
		-DWITH_BASISIMPORTER="$(usex basisuniversal ON OFF)" \
		-DWITH_DDSIMPORTER="$(usex dds ON OFF)" \
		-DWITH_DEVILIMAGEIMPORTER="$(usex devil ON OFF)" \
		-DWITH_DRFLACAUDIOIMPORTER="$(usex flac ON OFF)" \
		-DWITH_DRMP3AUDIOIMPORTER="$(usex flac ON OFF)" \
		-DWITH_DRWAVAUDIOIMPORTER="$(usex wav ON OFF)" \
		-DWITH_FAAD2AUDIOIMPORTER="$(usex wav ON OFF)" \
		-DWITH_FREETYPEFONT="$(usex freetype ON OFF)" \
		-DWITH_HARFBUZZFONT="$(usex harfbuzz ON OFF)" \
		-DWITH_JPEGIMAGECONVERTER="$(usex jpeg ON OFF)" \
		-DWITH_JPEGIMPORTER="$(usex jpeg ON OFF)" \
		-DWITH_MINIEXRIMAGECONVERTER="$(usex exr ON OFF)" \
		-DWITH_OPENGEXIMPORTER="$(usex gex ON OFF)" \
		-DWITH_PNGIMAGECONVERTER="$(usex png ON OFF)" \
		-DWITH_PNGIMPORTER="$(usex png ON OFF)" \
		-DWITH_STANFORDIMPORTER=OFF \
		-DWITH_STBIMAGECONVERTER=OFF \
		-DWITH_STBIMAGEIMPORTER=OFF \
		-DWITH_STBTRUETYPEFONT=OFF \
		-DWITH_STBVORBISAUDIOIMPORTER="$(usex vorbis ON OFF)" \
		-DWITH_TINYGLTFIMPORTER="$(usex tinygltf ON OFF)" \
		-G Ninja
	#)
	#BUILD_DIR=${WORKDIR}/${PF} cmake-utils_src_configure
}

src_compile() {
	cd "${WORKDIR}/${PF}/build"
	ninja
}

src_install() {
	cd "${WORKDIR}/${PF}/build"
	DESTDIR="${D}" eninja install
}
