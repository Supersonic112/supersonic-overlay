# Copyright 1999-2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

EGIT_REPO_URI="https://github.com/mosra/magnum.git"

inherit cmake-utils git-r3 ninja-utils

DESCRIPTION="C++11/C++14 graphics middleware for games and data visualization"
HOMEPAGE="http://magnum.graphics"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
# NOTE best practice: no use flags for important stuff
IUSE="+egl
+glfw
+glx
+magnumfont
+obj
+openal
+opengl
+png
+sdl2
+test
+tga
+wav
+windowlessglx"

RDEPEND="dev-libs/corrade
	glfw? ( media-libs/glfw )
	openal? ( media-libs/openal )
	opengl? ( virtual/opengl )
	png? ( media-libs/libpng )
	sdl2? ( media-libs/libsdl2 )"
DEPEND="${RDEPEND}
	dev-util/ninja"

CMAKE_MAKEFILE_GENERATOR="ninja"

src_configure() {
	mkdir -p "${WORKDIR}/${PF}/build"
	cd "${WORKDIR}/${PF}/build"
	#local mycmakeargs=(
	# NOTE maybe use shell array instead
	cmake .. \
		-DCMAKE_INSTALL_PREFIX="${EPREFIX}/usr" \
		-DCMAKE_BUILD_TYPE=Release \
		-DWITH_AUDIO=ON \
		-DWITH_GLXAPPLICATION="$(usex glx ON OFF)" \
		-DWITH_GLFWAPPLICATION="$(usex glfw ON OFF)" \
		-DWITH_SDL2APPLICATION="$(usex sdl2 ON OFF)" \
		-DWITH_WINDOWLESSGLXAPPLICATION="$(usex windowlessglx ON OFF)" \
		-DWITH_OPENGLTESTER="$(usex opengl $(usex test ON OFF) OFF)" \
		-DWITH_MAGNUMFONT="$(usex magnumfont ON OFF)" \
		-DWITH_MAGNUMFONTCONVERTER="$(usex magnumfont ON OFF)" \
		-DWITH_ANYIMAGECONVERTER=ON \
		-DWITH_ANYIMAGEIMPORTER=ON \
		-DWITH_IMAGECONVERTER=ON \
		-DWITH_ANYAUDIOIMPORTER=OFF \
		-DWITH_ANYSCENEIMPORTER=ON \
		-DWITH_OBJIMPORTER="$(usex obj ON OFF)" \
		-DWITH_TGAIMAGECONVERTER="$(usex tga ON OFF)" \
		-DWITH_TGAIMPORTER="$(usex tga ON OFF)" \
		-DWITH_WAVAUDIOIMPORTER="$(usex wav ON OFF)" \
		-DWITH_DISTANCEFIELDCONVERTER=ON \
		-DWITH_FONTCONVERTER=ON \
		-DWITH_IMAGECONVERTER=ON \
		-DWITH_EGLCONTEXT="$(usex egl ON OFF)" \
		-DWITH_GLXCONTEXT="$(usex glx ON OFF)" \
		-DWITH_GL_INFO="$(usex opengl ON OFF)" \
		-DWITH_AL_INFO="$(usex openal ON OFF)" \
		-DBUILD_TESTS=ON \
		-G Ninja
	#)
	#BUILD_DIR=${WORKDIR}/${PF} cmake-utils_src_configure
}

src_compile() {
	cd "${WORKDIR}/${PF}/build"
	ninja
}

src_install() {
	cd "${WORKDIR}/${PF}/build"
	DESTDIR="${D}" eninja install
}
