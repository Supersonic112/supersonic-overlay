# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

EGIT_REPO_URI="https://github.com/mosra/magnum-integration.git"

inherit cmake-utils git-r3

DESCRIPTION="Integration libraries for the Magnum C++11/C++14 graphics engine"
HOMEPAGE="http://magnum.graphics"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+bullet
	dart
	imgui
"

RDEPEND="
	dev-cpp/eigen
	dev-libs/magnum
	media-libs/glm
	bullet? ( sci-physics/bullet )
"
DEPEND="${RDEPEND}
	dev-util/ninja"

CMAKE_MAKEFILE_GENERATOR="ninja"

src_configure() {
	local mycmakeargs=(
		-DCMAKE_INSTALL_PREFIX="${EPREFIX}/usr"
		-DCMAKE_BUILD_TYPE=Release
		-DWITH_BULLET="$(usex bullet ON OFF)"
		-DWITH_DART="$(usex dart ON OFF)"
		-DWITH_EIGEN=ON
		-DWITH_GLM=ON
		-DWITH_IMGUI="$(usex imgui ON OFF)"
		-G Ninja
	)
	BUILD_DIR=${WORKDIR}/${PF} cmake-utils_src_configure
}

src_compile() {
	eninja
}

src_install() {
	DESTDIR="${D}" eninja install
}
