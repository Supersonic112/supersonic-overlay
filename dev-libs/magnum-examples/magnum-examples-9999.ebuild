# Copyright 1999-2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

EGIT_REPO_URI="https://github.com/mosra/magnum-examples.git"

inherit cmake-utils git-r3 ninja-utils

DESCRIPTION="Examples for the Magnum C++11/C++14 graphics engine"
HOMEPAGE="http://magnum.graphics"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+bullet"

RDEPEND="
	dev-libs/magnum
	dev-libs/magnum-plugins
	dev-libs/magnum-integration
	dev-libs/magnum-extras
	media-libs/openal
	bullet? ( sci-physics/bullet )
"
DEPEND="${RDEPEND}
	dev-util/ninja"

CMAKE_MAKEFILE_GENERATOR="ninja"

src_configure() {
	local mycmakeargs=(
		-DCMAKE_INSTALL_PREFIX="${EPREFIX}/usr"
		-DCMAKE_BUILD_TYPE=Debug
		-DWITH_AREALIGHTS_EXAMPLE=ON
		-DWITH_AUDIO_EXAMPLE=ON
		-DWITH_BULLET_EXAMPLE="$(usex bullet ON OFF)"
		-DWITH_CUBEMAP_EXAMPLE=ON
		-DWITH_MOTIONBLUR_EXAMPLE=ON
		-DWITH_PICKING_EXAMPLE=ON
		-DWITH_PRIMITIVES_EXAMPLE=ON
		-DWITH_SHADOWS_EXAMPLE=ON
		-DWITH_TEXT_EXAMPLE=ON
		-DWITH_TEXTUREDTRIANGLE_EXAMPLE=ON
		-DWITH_TRIANGLE_EXAMPLE=ON
		-DWITH_TRIANGLE_PLAIN_GLFW_EXAMPLE=OFF
		-DWITH_TRIANGLE_SOKOL_EXAMPLE=OFF
		-DWITH_VIEWER_EXAMPLE=ON
		-G Ninja
	)
	BUILD_DIR=${WORKDIR}/${PF} cmake-utils_src_configure
}

src_compile() {
	eninja
}

src_install() {
	DESTDIR="${D}" eninja install
}
